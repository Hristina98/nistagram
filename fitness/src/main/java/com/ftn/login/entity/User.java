package com.ftn.login.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="user", catalog="login")
public class User implements Serializable {
	
	private Integer id;
	private String username;
	private String password;
	private String fullName;
	private String email;
	private String mobile;
	private String gender;
	private Date birthDate;
	private String website;
	private String biography;
	private String role;
	private Boolean isPublic;
	private Boolean receiveMessages;
	private Boolean enableTags;
	private Boolean verified;
	
	
	public User() {
		
	}
	
	public User(Integer id, String username, String password, String fullName, String email, String mobile,
			String gender, Date birthDate, String website, String biography, String role, Boolean isPublic,
			Boolean receiveMessages, Boolean enableTags, Boolean verified) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.birthDate = birthDate;
		this.website = website;
		this.biography = biography;
		this.role = role;
		this.isPublic = isPublic;
		this.receiveMessages = receiveMessages;
		this.enableTags = enableTags;
		this.verified = verified;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "username", nullable = false, length = 45)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false, length = 45)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "role", nullable = false, length = 45)
	public String getRole() {
		return this.role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Column(name = "fullname", nullable = false, length = 45)
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	@Column(name = "email", nullable = false, length = 45)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name = "mobile", nullable = false, length = 45)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	@Column(name = "gender", nullable = false, length = 45)
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	@Column(name = "birthdate", nullable = true, length = 45)
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "website", nullable = false, length = 45)
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Column(name = "biography", nullable = true, length = 45)
	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	@Column(name = "isPublic", nullable = true, length = 45)
	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	@Column(name = "receiveMessages", nullable = true, length = 45)
	public Boolean getReceiveMessages() {
		return receiveMessages;
	}

	public void setReceiveMessages(Boolean receiveMessages) {
		this.receiveMessages = receiveMessages;
	}

	@Column(name = "enableTags", nullable = true, length = 45)
	public Boolean getEnableTags() {
		return enableTags;
	}

	public void setEnableTags(Boolean enableTags) {
		this.enableTags = enableTags;
	}

	@Column(name = "verified", nullable = true, length = 45)
	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	
	

}
