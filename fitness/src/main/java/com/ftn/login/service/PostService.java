package com.ftn.login.service;

import java.util.List;

import com.ftn.login.entity.Post;
import com.ftn.login.entity.User;

public interface PostService {
	
	
	 List<Post> findPosts();

}
