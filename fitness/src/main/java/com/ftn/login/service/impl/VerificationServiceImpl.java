package com.ftn.login.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.login.dao.user.UserDao;
import com.ftn.login.dao.user.VerificationDao;
import com.ftn.login.entity.Verification;
import com.ftn.login.service.VerificationService;

@Service
public class VerificationServiceImpl implements VerificationService{
	
	@Autowired
	private VerificationDao verificationDao;

	@Override
	public Boolean verify(Verification req) {
		return verificationDao.verify(req);
	}

}
