package com.ftn.login.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.login.dao.user.UserDao;
import com.ftn.login.entity.User;
import com.ftn.login.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	public User findUser(String username, String password) {
		return userDao.findUser(username,password);
	}

	@Override
	public User addUser(User user) {
		return userDao.addUser(user);
	}

}
