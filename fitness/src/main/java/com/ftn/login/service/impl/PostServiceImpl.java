package com.ftn.login.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.login.dao.user.PostDao;
import com.ftn.login.entity.Post;

import com.ftn.login.service.PostService;


@Service
public class PostServiceImpl  implements PostService{

	
	
	@Autowired
	private PostDao postDao;

	@Override
	public  List<Post> findPosts() {
		return postDao.findPosts();
	}
}
