package com.ftn.login.service;

import com.ftn.login.entity.Verification;

public interface VerificationService {

	Boolean verify(Verification req);

}
