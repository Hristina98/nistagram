package com.ftn.login.service;


import com.ftn.login.entity.User;
import com.ftn.login.entity.Verification;

public interface UserService {

	User findUser(String username, String password);

	User addUser(User user);

}
