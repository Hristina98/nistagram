package com.ftn.login.dao;

public class DaoException extends RuntimeException {

	private static final long serialVersionUID = 2487481264684207463L;
	
	public DaoException(String message) {
		super(message);
	}

}
