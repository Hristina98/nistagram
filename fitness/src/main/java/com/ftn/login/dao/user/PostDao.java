package com.ftn.login.dao.user;

import java.util.List;

import com.ftn.login.dao.GenericDao;
import com.ftn.login.entity.Post;


public interface PostDao extends GenericDao<Post, Integer> {

	 List<Post> findPosts();

}
