package com.ftn.login.dao.user;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.login.dao.AbstractGenericDao;
import com.ftn.login.entity.User;
import com.ftn.login.entity.Verification;

@Repository
@Transactional
public class VerificationDaoImpl extends AbstractGenericDao<Verification, Integer> implements VerificationDao{

	@Autowired
	public VerificationDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean verify(Verification req) {
		insert(req);
		return true;
	}

}
