package com.ftn.login.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public abstract class AbstractGenericDao<T,PK> implements GenericDao<T, PK> {

	protected SessionFactory sessionFactory;
	private Logger logger;
	
	private Class<T> type;
	private Class<? extends Serializable> pkType;
	
	public AbstractGenericDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		logger = Logger.getLogger(AbstractGenericDao.class);
		resolveGenericType();
	}
	
	@SuppressWarnings("unchecked")
	private void resolveGenericType() {
		Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class<T>) pt.getActualTypeArguments()[0];
        pkType = (Class<? extends Serializable>) pt.getActualTypeArguments()[1];
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() throws DaoException {
		try {
			return sessionFactory.getCurrentSession().createCriteria(type).list();
		} catch (HibernateException e) {
			logger.info(e);
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public T get(PK id) throws DaoException {
		try {
			return sessionFactory.getCurrentSession().get(type, pkType.cast(id));
		} catch (HibernateException e) {
			logger.info(e);
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public T insert(T entity) throws DaoException {
		return saveOrUpdate(entity);
	}

	@Override
	public T update(T entity) throws DaoException {
		return saveOrUpdate(entity);
	}

	@Override
	public void delete(T entity) throws DaoException {
		try {
			sessionFactory.getCurrentSession().delete(entity);
		} catch (HibernateException e) {
			logger.info(e);
			throw new DaoException(e.getMessage());
		}
	}
	
	private T saveOrUpdate(T entity) {
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(entity);
			return entity;
		} catch (HibernateException e) {
			logger.info(e);
			throw new DaoException(e.getMessage());
		}
	}


}
