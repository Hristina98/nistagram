package com.ftn.login.dao.user;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.login.dao.AbstractGenericDao;
import com.ftn.login.entity.Post;
import com.ftn.login.entity.User;

@Repository
@Transactional
public class PostDaoImpl  extends AbstractGenericDao<Post, Integer> implements PostDao {

	
	@Autowired
	public PostDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	
	
	
	@Override
	public List<Post> findPosts() {
		
	
		
		Query q = sessionFactory.getCurrentSession()
				.createQuery("SELECT p FROM Post p inner join p.user u where u.isPublic = true"); 																					

		List<Post> posts = q.list();
		
		
		
		return posts;
		

	}
}
