package com.ftn.login.dao;

import java.util.List;

public interface GenericDao<T,PK> {

	List<T> getAll() throws DaoException;
	
	T get(PK id) throws DaoException;
	
	T insert(T entity) throws DaoException;
	
	T update(T entity) throws DaoException;
	
	void delete(T entity) throws DaoException;
}
