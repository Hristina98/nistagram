package com.ftn.login.dao.user;

import com.ftn.login.dao.GenericDao;
import com.ftn.login.entity.User;
import com.ftn.login.entity.Verification;

public interface VerificationDao extends GenericDao<Verification, Integer>{

	Boolean verify(Verification req);

}
