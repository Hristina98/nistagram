package com.ftn.login.dao.user;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.login.dao.AbstractGenericDao;
import com.ftn.login.dao.DaoException;
import com.ftn.login.entity.User;

@Repository
@Transactional
public class UserDaoImpl extends AbstractGenericDao<User, Integer> implements UserDao {
	
	@Autowired
	public UserDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public User findUser(String username, String password) {
		
		//System.out.println(username);
		//System.out.println(password);
		
		Query q = sessionFactory.getCurrentSession()
				.createQuery("SELECT u FROM User u WHERE u.username like :Username and u.password like :Password"); 																					// emaill
		q.setParameter("Username", username);
		q.setParameter("Password", password);
		
		

		User u = (User) q.uniqueResult();
		
		System.out.println(u.getFullName());
		
		return u;
		
		//UserDTO userDTO = new UserDTO(u.getId(), u.getUsername(), u.getPassword());
		
		//return userDTO;
	}

	@Override
	public User addUser(User user) {
		insert(user);
		
		return user;
	}

}
