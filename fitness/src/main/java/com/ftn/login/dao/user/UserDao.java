package com.ftn.login.dao.user;

import com.ftn.login.dao.GenericDao;
import com.ftn.login.entity.User;

public interface UserDao extends GenericDao<User, Integer>{

	User findUser(String username, String password);
	
	User addUser(User user);

}
