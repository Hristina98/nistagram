package com.ftn.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.login.entity.User;
import com.ftn.login.entity.Verification;
import com.ftn.login.service.UserService;

import org.springframework.http.MediaType;

@RestController
@CrossOrigin
public class LoginController {
	
	@Autowired
	private UserService userService;
	
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginController.class);    
    
    @RequestMapping(value = "/login/{username}/{password}")
	public ResponseEntity<?> findUser(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		//BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		return new ResponseEntity<User>(userService.findUser(username, password), HttpStatus.OK);

	}
    @RequestMapping(value = "/signup/addUser/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> addUser(@RequestBody User user) {
  		//BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
  		
  		return new ResponseEntity<User>(userService.addUser(user), HttpStatus.OK);

  	}
    
    


}

