package com.ftn.login.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.login.entity.Post;
import com.ftn.login.service.PostService;

@RestController
@CrossOrigin
public class PostController {
	
	
	@Autowired
	private PostService postService;
	
	@RequestMapping(value = "/post")
	public ResponseEntity<?> findAllPosts() {
			
		return new ResponseEntity< List<Post>>(postService.findPosts(), HttpStatus.OK);

	}
	

}
