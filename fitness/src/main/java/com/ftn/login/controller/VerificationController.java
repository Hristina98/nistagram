package com.ftn.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.login.entity.Verification;
import com.ftn.login.service.UserService;
import com.ftn.login.service.VerificationService;

@RestController
@CrossOrigin
public class VerificationController {
	
	@Autowired
	private VerificationService verificationService;
	
	@RequestMapping(value = "/verify", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> verifyProfile(@RequestBody Verification req) {
  		
  		return new ResponseEntity<Boolean>(verificationService.verify(req), HttpStatus.OK);

  	}

}
