package com.ftn.fitness.service;

import java.util.List;

import com.ftn.fitness.entity.AppointmentTerm;
import com.ftn.fitness.entity.Member;

public interface AppointmentService{

	public List<AppointmentTerm> findAppointments();



}
