package com.ftn.fitness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.fitness.dao.user.MealDao;
import com.ftn.fitness.dao.user.MemberDao;
import com.ftn.fitness.entity.Meal;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.service.MealService;
import com.ftn.fitness.service.MemberService;


@Service
public class MealServiceImpl implements MealService{

	
	@Autowired
	private MealDao mealDao;

	@Override
	public Meal addMeal(Meal meal) {
		return mealDao.addMeal(meal);
	}
	
	
	
	
	@Override
	public List<Meal> findMeals()
	{
		return mealDao.findMeals();
	}

}




