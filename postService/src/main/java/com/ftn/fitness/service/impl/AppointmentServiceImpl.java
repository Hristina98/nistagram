package com.ftn.fitness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.fitness.dao.user.AppointmentDao;
import com.ftn.fitness.dao.user.MemberDao;
import com.ftn.fitness.entity.AppointmentTerm;
import com.ftn.fitness.service.AppointmentService;;

@Service
public class AppointmentServiceImpl  implements AppointmentService {

	@Autowired
	private AppointmentDao appointmentDao;
	
	
	
	@Override
	public List<AppointmentTerm> findAppointments()
	{
		return appointmentDao.findAppointments();
	}

	
}
