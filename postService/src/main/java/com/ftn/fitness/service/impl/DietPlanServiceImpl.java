package com.ftn.fitness.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.fitness.dao.user.DietPlanDao;
import com.ftn.fitness.dao.user.MemberDao;
import com.ftn.fitness.entity.DietPlan;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.service.DietPlanService;
import com.ftn.fitness.service.MemberService;



@Service
public class DietPlanServiceImpl implements DietPlanService {


		
		@Autowired
		private DietPlanDao dietPlanDao;

		@Override
		public DietPlan addPlan(DietPlan dietPlan) {
			return dietPlanDao.addPlan(dietPlan);
		}
}
