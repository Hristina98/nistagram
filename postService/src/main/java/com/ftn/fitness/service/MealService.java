package com.ftn.fitness.service;

import java.util.List;

import com.ftn.fitness.entity.Meal;
import com.ftn.fitness.entity.Member;

public interface MealService {

	public Meal addMeal(Meal meal);

	public List<Meal> findMeals();


}



