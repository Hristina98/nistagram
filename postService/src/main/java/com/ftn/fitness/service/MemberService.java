package com.ftn.fitness.service;

import java.util.List;

import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;

public interface MemberService {

	public Member addMember(Member member);

	public List<Member> findMembers();

}
