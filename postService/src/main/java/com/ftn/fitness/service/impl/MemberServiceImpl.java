package com.ftn.fitness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.fitness.dao.user.MemberDao;
import com.ftn.fitness.dao.user.UserDao;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;
import com.ftn.fitness.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {
	
	@Autowired
	private MemberDao memberDao;

	@Override
	public Member addMember(Member member) {
		return memberDao.addMember(member);
	}
	
	
	@Override
	public List<Member> findMembers()
	{
		return memberDao.findMembers();
	}

}
