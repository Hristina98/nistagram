package com.ftn.fitness.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dietPlan", catalog="fitness")
public class DietPlan {

	
	private Integer id;
	private String breakfast;
	private String lunch;
	private String snack;
	private String dinner;
	private Integer water;
	
	private Date date;

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "BREAKFAST", nullable = false, length = 45)
	public String getBreakfast() {
		return breakfast;
	}

	public void setBreakfast(String breakfast) {
		this.breakfast = breakfast;
	}

	
	@Column(name = "LUNCH", nullable = false, length = 45)
	public String getLunch() {
		return lunch;
	}

	public void setLunch(String lunch) {
		this.lunch = lunch;
	}

	
	@Column(name = "SNACK", nullable = false, length = 45)
	public String getSnack() {
		return snack;
	}

	public void setSnack(String snack) {
		this.snack = snack;
	}

	
	@Column(name = "DINNER", nullable = false, length = 45)
	public String getDinner() {
		return dinner;
	}

	public void setDinner(String dinner) {
		this.dinner = dinner;
	}

	
	@Column(name = "WATER", nullable = false, length = 45)
	public Integer getWater() {
		return water;
	}

	public void setWater(Integer water) {
		this.water = water;
	}

	
	@Column(name = "DATE", nullable = false, length = 45)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
