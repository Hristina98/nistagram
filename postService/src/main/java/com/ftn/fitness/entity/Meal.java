package com.ftn.fitness.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="meal", catalog="fitness")
public class Meal implements Serializable {
	
	private Integer id;
	private Integer calories;
	private Integer proteins;
	private Integer carbs;
	private Integer fats;
	
	
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "CALORIES", nullable = false, length = 45)
	public Integer getCalories() {
		return calories;
	}
	public void setCalories(Integer calories) {
		this.calories = calories;
	}
	
	@Column(name = "PROTEINS", nullable = false, length = 45)
	public Integer getProteins() {
		return proteins;
	}
	public void setProteins(Integer proteins) {
		this.proteins = proteins;
	}
	
	@Column(name = "CARBS", nullable = false, length = 45)
	public Integer getCarbs() {
		return carbs;
	}
	public void setCarbs(Integer carbs) {
		this.carbs = carbs;
	}
	
	@Column(name = "FATS", nullable = false, length = 45)
	public Integer getFats() {
		return fats;
	}
	public void setFats(Integer fats) {
		this.fats = fats;
	}
	
	@Column(name = "NAME", nullable = false, length = 45)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "DESCRIPTION", nullable = false, length = 45)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	private String name;
	private String description;


	
	
}
