package com.ftn.fitness.dao.user;

import com.ftn.fitness.dao.GenericDao;
import com.ftn.fitness.entity.User;

public interface UserDao extends GenericDao<User, Integer>{

	User findUser(String username, String password);
	
	User findUserByUsername(String username);

	User addUser(User user);

}
