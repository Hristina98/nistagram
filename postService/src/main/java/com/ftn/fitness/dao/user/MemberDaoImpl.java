package com.ftn.fitness.dao.user;

import java.util.List;
import org.hibernate.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.AbstractGenericDao;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;

@Repository
@Transactional
public class MemberDaoImpl  extends AbstractGenericDao<Member, Integer> implements MemberDao  {
	
	@Autowired
	public MemberDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public Member addMember(Member member) {
		member.setAbsenceCounter(0);
		member.setBMI(0.0);
		insert(member);
		
		return member;
	}
	
	
	
	@Override
	public List<Member> findMembers()
	{
		Query q = sessionFactory.getCurrentSession().createQuery("SELECT DISTINCT m FROM Member m");

		List<Member> members = q.list();
		return members;
		
	}

	

}
