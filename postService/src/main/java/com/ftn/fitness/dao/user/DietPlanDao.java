package com.ftn.fitness.dao.user;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.GenericDao;
import com.ftn.fitness.entity.DietPlan;
import com.ftn.fitness.entity.Member;


@Repository
@Transactional
public interface DietPlanDao extends GenericDao<DietPlan, Integer> {


		public DietPlan addPlan(DietPlan dietPlan);



}
