package com.ftn.fitness.dao.user;
import com.ftn.fitness.dao.GenericDao;
import com.ftn.fitness.entity.Meal;
import com.ftn.fitness.entity.Member;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface MealDao extends GenericDao<Meal, Integer> {

	
	public Meal addMeal(Meal meal);
	
	public List<Meal> findMeals();
}




