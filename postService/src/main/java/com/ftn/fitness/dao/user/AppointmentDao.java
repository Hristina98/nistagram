package com.ftn.fitness.dao.user;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.GenericDao;
import com.ftn.fitness.entity.AppointmentTerm;
import com.ftn.fitness.entity.Member;



@Repository
@Transactional
public interface AppointmentDao  extends GenericDao<AppointmentTerm, Integer> {

	
	public List<AppointmentTerm> findAppointments();
}
