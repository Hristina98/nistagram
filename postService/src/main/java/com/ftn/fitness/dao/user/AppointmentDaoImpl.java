package com.ftn.fitness.dao.user;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.AbstractGenericDao;
import com.ftn.fitness.entity.AppointmentTerm;
import com.ftn.fitness.entity.Member;



@Repository
@Transactional
public class AppointmentDaoImpl extends AbstractGenericDao<AppointmentTerm, Integer> implements AppointmentDao  {
	
	@Autowired
	public AppointmentDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public List<AppointmentTerm> findAppointments(){
		Query q = sessionFactory.getCurrentSession().createQuery("SELECT DISTINCT m FROM AppointmentTerm m");

		List<AppointmentTerm> terms = q.list();
		
		
		return terms;
	}


}
