package com.ftn.fitness.dao.user;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.AbstractGenericDao;
import com.ftn.fitness.entity.DietPlan;
import com.ftn.fitness.entity.Member;



@Repository
@Transactional
public class DietPlanDaoImpl extends AbstractGenericDao<DietPlan, Integer> implements DietPlanDao{

	
	

		
		@Autowired
		public DietPlanDaoImpl(SessionFactory sessionFactory) {
			super(sessionFactory);
		}

		@Override
		public DietPlan addPlan(DietPlan dietPlan) {
			insert(dietPlan);
			
			return dietPlan;
		}
		
}
