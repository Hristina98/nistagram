package com.ftn.fitness.dao.user;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.AbstractGenericDao;
import com.ftn.fitness.entity.Meal;
import com.ftn.fitness.entity.Member;

@Repository
@Transactional
public class MealDaoImpl extends AbstractGenericDao<Meal, Integer> implements MealDao  {
	
	
	@Autowired
	public MealDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public Meal addMeal(Meal meal) {
		insert(meal);
		return meal;
	}
	
	

	@Override
	public List<Meal> findMeals()
	{
		Query q = sessionFactory.getCurrentSession().createQuery("SELECT DISTINCT m FROM Meal m");

		List<Meal> meals = q.list();
		return meals;
		
	}

}





	