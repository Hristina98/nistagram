package com.ftn.fitness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;
import com.ftn.fitness.service.MemberService;
import com.ftn.fitness.service.UserService;

@RestController
@CrossOrigin
public class MemberController {
	
	@Autowired
	private MemberService memberService;
	
	@RequestMapping(value = "/signup/addMember/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> addMember(@RequestBody Member member) {

  		
  		return new ResponseEntity<Member>(memberService.addMember(member), HttpStatus.OK);

  	}
	
	
	
	
	
	@RequestMapping(value = "/findMembers/")
	public ResponseEntity<?> findMembers()  {
		return new ResponseEntity<List<Member>>(memberService.findMembers(), HttpStatus.OK);
	}


}
