package com.ftn.fitness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.fitness.entity.AppointmentTerm;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.service.AppointmentService;
import com.ftn.fitness.service.MemberService;

@RestController
@CrossOrigin
public class AppointmentController {

	
	@Autowired
	private AppointmentService appointmentService;
	

	
	
	
	
	@RequestMapping(value = "/findAppointments/")
	public ResponseEntity<?> findAppointments()  {
		return new ResponseEntity<List<AppointmentTerm>>(appointmentService.findAppointments(), HttpStatus.OK);
	}
}
