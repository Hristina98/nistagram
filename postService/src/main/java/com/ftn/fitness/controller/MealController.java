package com.ftn.fitness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.fitness.entity.Meal;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.service.MealService;
import com.ftn.fitness.service.UserService;

@RestController
@CrossOrigin
public class MealController {

	
	
	@Autowired
	private MealService mealService;
	
	
	
	
	@RequestMapping(value = "/addMeal/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> addMeal(@RequestBody Meal meal) {

  		
  		return new ResponseEntity<Meal>(mealService.addMeal(meal), HttpStatus.OK);

  	}
	
	

	@RequestMapping(value = "/findMeals/")
	public ResponseEntity<?> findMeals()  {
		return new ResponseEntity<List<Meal>>(mealService.findMeals(), HttpStatus.OK);
	}

	
}
