package com.ftn.fitness.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ftn.fitness.entity.DietPlan;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.service.DietPlanService;
import com.ftn.fitness.service.MemberService;

public class DietPlanController {
	
	@Autowired
	private DietPlanService dietPlanService;
	
	@RequestMapping(value = "/addPlan/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> addPlan(@RequestBody DietPlan dietPlan) {

  		
  		return new ResponseEntity<DietPlan>(dietPlanService.addPlan(dietPlan), HttpStatus.OK);

  	}

}
