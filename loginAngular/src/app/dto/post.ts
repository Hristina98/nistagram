import { User } from "./user";

export class Post{
    id: Number;
    user : User;
    imagePath : String;
    description : String;
}