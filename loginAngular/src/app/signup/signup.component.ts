import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { User } from '../dto/user';
import { Endpoint } from '../util/endpoints-enum';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    username: String;
    password: String;
    fullName: String;
    email: String;
    mobile: String;
    gen: number;
    gender: String;
    birthDate = new Date();
    website: String;
    biography: String;
    endpoint = Endpoint;
    

  constructor(private router: Router,private http: HttpClient) { }

  ngOnInit(): void {
  }

  
  
  signUp(){
    let user: User = new User
    user.username = this.username
    user.password = this.password
    user.biography = this.biography   
    user.birthDate = this.birthDate
    user.email = this.email
    user.fullName = this.fullName
    user.mobile = this.mobile
    if(this.gender == 'option1')
        user.gender = 'male'
    else
        user.gender = 'female'
    user.website = this.website
    user.role = 'nistagramer'
    user.isPublic = false
    user.receiveMessages = true
    user.enableTags = true
    user.verified = false

    const headers = { 'content-type': 'application/json'}  // da bi odgovaralo json-u
    const body=JSON.stringify(user);   //konverzija objekta subscriber u json
    
    let options = { headers: headers };
    this.http.post<any>(this.endpoint.LOGIN_DEV+'signup/addUser/', body, options).pipe(
      map(returnedPersonId => {
        //after sign up redirect to login
        if(confirm("Successfully created account.")) {
          this.router.navigate(["login"]);}

})
    ).subscribe()
  }
}
