import { CloseScrollStrategyConfig } from '@angular/cdk/overlay/scroll/close-scroll-strategy';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Verification } from '../dto/verification';
import { Endpoint } from '../util/endpoints-enum';
import { Global } from '../util/global';

@Component({
  selector: 'app-verify-profile',
  templateUrl: './verify-profile.component.html',
  styleUrls: ['./verify-profile.component.css']
})
export class VerifyProfileComponent implements OnInit {

  category: String; 
  
  endpoint = Endpoint;
  
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
  }

  onSendRequest(){
    let req: Verification = new Verification()
    req.category = this.category
    req.userId = Global.loggedUser.id
    req.image = 'random.jpg'
    const headers = { 'content-type': 'application/json'}  // da bi odgovaralo json-u
    const body=JSON.stringify(req);   //konverzija objekta subscriber u json
    
    let options = { headers: headers };
    this.http.post<any>(this.endpoint.LOGIN_DEV+'verify', body, options).pipe(
      map(returnedPersonId => {
        alert("Successfuly sent verification request.")
        this.router.navigate(["loggedNistagramer"]);

})
    ).subscribe()

  }

}
