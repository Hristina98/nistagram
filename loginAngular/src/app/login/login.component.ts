import { DatePipe, SlicePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Endpoint } from '../util/endpoints-enum';
import { Global } from '../util/global';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  endpoint = Endpoint;

  constructor(private router: Router,private http: HttpClient) { }

  ngOnInit(): void {
  }

  login() : void {
    this.http
      .get(this.endpoint.LOGIN_DEV+'login/' + this.username + "/" + this.password)
      .pipe(
        map(returnedUser => {
          Global.loggedUser.id = returnedUser["id"]
          Global.loggedUser.username = returnedUser["username"]
          Global.loggedUser.password = returnedUser["password"]
          Global.loggedUser.biography = returnedUser["biography"] 
          
          var datePipe = new DatePipe('en-US'); 
          let newDate = new Date(datePipe.transform(returnedUser["birthDate"], 'dd-MM-yyyy'));
          Global.loggedUser.birthDate = newDate
          Global.loggedUser.email = returnedUser["email"]
          Global.loggedUser.fullName = returnedUser["fullName"]
          Global.loggedUser.mobile = returnedUser["mobile"]
          Global.loggedUser.gender = returnedUser["gender"]
          Global.loggedUser.website = returnedUser["website"]
          Global.loggedUser.role = returnedUser["role"]
          Global.loggedUser.isPublic = returnedUser["isPublic"]
          Global.loggedUser.receiveMessages = returnedUser["receiveMessages"]
          Global.loggedUser.enableTags = returnedUser["enableTags"]
            if(returnedUser != undefined)
                if(returnedUser["role"]=="admin"){
                  this.router.navigate(["loggedAdmin"]);
                }else if(returnedUser["role"]=="agent"){
                  this.router.navigate(["loggedAgent"]);
                }else{
                  this.router.navigate(["loggedNistagramer"]);
                }
              
              else
                alert("Invalid credentials")
        })
      ).subscribe()
      
  }

 

}


